### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Add Kubernetes cluster to the gitlab project

Operations -> Kubernetes -> Connect Cluster With Certificate

Provide the following fields:

`NAME` (name of the cluster for gitlab project)

`API URL` (.kube/config -> cluster.server)

`CA CERTIFICATE` (.kube/config -> cluster.certificate-authority-data (decode from base64))

### Variables

`KUBE_INGRESS_BASE_DOMAIN` - your domain. You should also have 
a CNAME record referencing to this domain as 3rd level domain, because gitlab-ci will
create domains like: your-project.your-domain.com

`STAGING_ENABLED` - should be provided if you want gitlab-ci to deploy a staging for you
in the same kubernetes cluster

#### Set Variables for environment in .gitlab-ci.yml
See .gitlab-ci.yml for example
